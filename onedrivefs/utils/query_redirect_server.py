from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs


class QueryRedirectServer(HTTPServer):
    """Simple server for web api callback

    Args:
        port: server port
    """

    def __init__(self, port):
        HTTPServer.__init__(self, ('0.0.0.0', port), self._QueryRequestHandler)

    def get_resp(self):
        """Get one request and close server

        Return:
            url query as a dict
        """

        self.handle_request()
        self.server_close()
        return self.query

    class _QueryRequestHandler(BaseHTTPRequestHandler):
        def do_GET(self):
            # parse url
            parsed_url = urlparse(self.path)
            self.server.query = {
                k: vs[0]
                for k, vs in parse_qs(parsed_url.query).items()
            }

            # send response
            self.send_response(200)
            self.end_headers()
            self.wfile.write("Got it".encode())


def get_callback_result(port=80, url=None):
    """Init query redirect server and get result

    Args:
        port: server port
        url: server url
             if url is not None, it will ignore port arg
    """

    if url is not None:
        try:
            port = int(url.split(':')[-1])
        except ValueError:
            pass

    return QueryRedirectServer(port).get_resp()
