import onedrivesdk
from .utils.query_redirect_server import get_callback_result

import re
from os.path import expanduser, dirname, basename
import logging
import cachetools
import operator


def _arg_getter(index, key):
    def _getter(*args, **kwargs):
        try:
            return kwargs[key]
        except KeyError:
            return args[index]

    return _getter


class OneDriveClient:
    SCOPES = ['wl.signin', 'wl.offline_access', 'onedrive.readwrite']
    SESSION_PATH = expanduser("~/.onedrivefs")

    logger = logging.getLogger(__name__)

    def __init__(self, client_id, client_secret, redirect_url):
        self._client_id = client_id
        self._client_secret = client_secret
        self._redirect_url = redirect_url

        self._id_path_cache = cachetools.LFUCache(100)
        self._path_id_cache = cachetools.LFUCache(100)

        self._delta_token = 'latest'

        self._client = onedrivesdk.get_default_client(
            client_id=self._client_id, scopes=OneDriveClient.SCOPES)
        try:
            self.logger.info("try to load session file")
            self._client.auth_provider.load_session(
                    path=OneDriveClient.SESSION_PATH)
        except FileNotFoundError:
            self.logger.info("no such session file")
            pass  # no session file

    def auth(self):
        "Get a new auth code and redeem access token & refresh token"

        url = self._client.auth_provider.get_auth_url(self._redirect_url)

        print("Open this URL in your browser: " + url)
        result = get_callback_result(url=self._redirect_url)

        try:
            self._client.auth_provider.authenticate(
                result['code'], self._redirect_url, self._client_secret)
            self._client.auth_provider.save_session(
                path=OneDriveClient.SESSION_PATH)
        except KeyError:  # auth error
            raise(ApiError(result['error'], result['error_description']))
        except Exception as e:  # redeem error
            if str(e) == 'invalid_grant':
                raise(ApiError(e, 'May be the auth code is invalid.'))

    def refresh_token(self):
        "Refresh access token"

        try:
            self._client.auth_provider.refresh_token()
        except Exception as e:
            if str(e) == 'invalid_grant':
                raise(ApiError(e, 'May be the auth code is invalid.'))

    def check_auth(self):
        "If refresh access token successfully"

        try:
            self.refresh_token()
            return True
        except ApiError:
            return False

    def readdir(self, path):
        try:
            return list(
                map(lambda x: x.name,
                    self._client.item(path=path).children.get()))
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(path))
            else:
                raise(e)

    def download(self, path, dst):
        try:
            self._client.item(path=path).download(dst)
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(path))
            else:
                raise(e)

    def upload(self, path, src):
        base_dir, name = OneDriveClient._split_dir_and_base(path)
        base_dir = OneDriveClient._path_add_root(base_dir)
        self._client.item(path=base_dir).children[name].upload(src)

    def mkdir(self, path):
        base_dir, name = OneDriveClient._split_dir_and_base(path)
        base_dir = OneDriveClient._path_add_root(base_dir)

        f = onedrivesdk.Folder()
        i = onedrivesdk.Item()
        i.name = name
        i.folder = f

        return self._client.item(path=base_dir).children.add(i)

    def rename(self, old, new):
        dst_req = self._client.item(path=old)

        base_dir, name = OneDriveClient._split_dir_and_base(new)
        base_dir = OneDriveClient._path_add_root(base_dir)

        new_model = onedrivesdk.Item()
        new_model.name = name
        new_model.parent_reference = onedrivesdk.ItemReference()

        try:
            new_model.parent_reference.id = \
                self._client.item(path=base_dir).get().id
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(new))
            else:
                raise(e)

        try:
            dst_req.update(new_model)
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.NameAlreadyExists:
                raise(FileExistsError(new))
            elif e.code == onedrivesdk.error.ErrorCode.InvalidRequest:
                raise(FileNotFoundError(old))
            else:
                raise(e)

    def delete(self, path):
        try:
            self._client.item(path=path).delete()
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(path))
            else:
                raise(e)

    def delta(self):
        delta = self._client.item(path='/').delta(self._delta_token).get()
        self._delta_token = delta.token

        delta_items = []
        for item in delta:
            delta_items.append({
                'name': item.name,
                'id': item.id,
                'parent_id': item.parent_reference.id,
                'deleted': True if item.deleted else False
            })
        return delta_items

    @cachetools.cachedmethod(
        operator.attrgetter("_path_id_cache"),
        key=_arg_getter(1, 'path'))
    def _get_id_from_path(self, path):
        try:
            item_id = self._client.item(path=path).get().id
            self._id_path_cache[item_id] = path

            return item_id
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(path))
            else:
                raise(e)

    @cachetools.cachedmethod(
            operator.attrgetter("_id_path_cache"),
            key=_arg_getter(1, 'item_id'))
    def _get_path_from_id(self, item_id):
        try:
            item = self._client.item(id=item_id).get()
            parent_path = item.parent_reference.path
            name = item.name

            if parent_path is None and name == 'root':
                path = '/'
            else:
                path = parent_path.split(':')[1] + '/' + name

            self._path_id_cache[path] = item_id

            return path
        except onedrivesdk.error.OneDriveError as e:
            if e.code == onedrivesdk.error.ErrorCode.ItemNotFound:
                raise(FileNotFoundError(item_id))
            else:
                raise(e)

    @staticmethod
    def _path_add_root(path):
        return re.sub('^/{0,1}', '/', path)

    @staticmethod
    def _split_dir_and_base(path):
        return (dirname(path), basename(path))


class ApiError(RuntimeError):
    def __init__(self, error, error_description='none'):
        self.error = error
        self.error_description = error_description

    def __str__(self):
        return '{}: {}'.format(self.error, self.error_description)
